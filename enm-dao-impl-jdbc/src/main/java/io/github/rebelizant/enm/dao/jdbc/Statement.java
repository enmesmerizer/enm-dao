package io.github.rebelizant.enm.dao.jdbc;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public interface Statement<T> {

    T execute();

}
