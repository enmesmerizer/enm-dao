package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public class PhraseCrudDaoJdbc implements PhraseCrudDao {
    @Override
    public Optional<AbstractPhrase> findById(Long id) {
        return null;
    }

    @Override
    public void save(AbstractPhrase phrase) {

    }

    @Override
    public void save(Collection<AbstractPhrase> phrases) {

    }

    @Override
    public void delete(AbstractPhrase phrase) {

    }

    @Override
    public void delete(Collection<AbstractPhrase> phrases) {

    }
}
