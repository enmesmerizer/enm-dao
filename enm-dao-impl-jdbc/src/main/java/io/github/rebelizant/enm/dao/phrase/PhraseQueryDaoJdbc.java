package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public class PhraseQueryDaoJdbc implements PhraseQueryDao {
    @Override
    public Optional<AbstractPhrase> findById(long id, Language language) {
        return null;
    }

    @Override
    public Optional<AbstractPhrase> findPhrase(String phraseValue, PartOfSpeech partOfSpeech, Language language) {
        return null;
    }

    @Override
    public Collection<AbstractPhrase> findPhrases(String phraseValue, Language language) {
        return null;
    }
}
