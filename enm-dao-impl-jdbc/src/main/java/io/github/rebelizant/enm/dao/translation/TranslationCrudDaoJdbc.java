package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.translation.AbstractTranslation;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public class TranslationCrudDaoJdbc implements TranslationCrudDao {
    @Override
    public void save(Collection<AbstractTranslation> translations) {

    }

    @Override
    public Optional<AbstractTranslation> findById(Long id) {
        return null;
    }

    @Override
    public void save(AbstractTranslation translation) {

    }

    @Override
    public void delete(Collection<AbstractTranslation> translations) {

    }

    @Override
    public void delete(AbstractTranslation translation) {

    }
}
