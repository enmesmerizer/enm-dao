package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;
import io.github.rebelizant.enm.core.translation.AbstractTranslation;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public class TranslationQueryDaoJdbc implements TranslationQueryDao {
    @Override
    public Optional<AbstractTranslation> findById(Long id, Language srcLang, Language destLang) {
        return null;
    }

    @Override
    public Collection<AbstractTranslation> findTranslations(String phraseValue, Language srcLang, Language destLang) {
        return null;
    }

    @Override
    public Collection<AbstractTranslation> findTranslations(String phraseValue, PartOfSpeech partOfSpeech, Language srcLang, Language destLang) {
        return null;
    }
}
