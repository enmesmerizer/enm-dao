package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.dao.CrudDao;

/**
 * @author rebelizant
 *         Created on 20.02.16.
 */
public interface PhraseCrudDao extends CrudDao<AbstractPhrase> {

}
