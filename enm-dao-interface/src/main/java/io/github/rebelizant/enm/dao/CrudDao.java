package io.github.rebelizant.enm.dao;

import io.github.rebelizant.enm.core.AbstractEntity;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public interface CrudDao<T extends AbstractEntity> {

    Optional<T> findById(Long id);

    void save(T entity);

    void save(Collection<T> entities);

    void delete(T entity);

    void delete(Collection<T> entities);

}
