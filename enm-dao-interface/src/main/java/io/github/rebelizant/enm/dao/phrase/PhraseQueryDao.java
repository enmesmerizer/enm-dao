package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 19.03.16.
 */
public interface PhraseQueryDao {

    Optional<AbstractPhrase> findById(long id, Language language);

    Optional<AbstractPhrase> findPhrase(String phraseValue, PartOfSpeech partOfSpeech, Language language);

    Collection<AbstractPhrase> findPhrases(String phraseValue, Language language);

}
