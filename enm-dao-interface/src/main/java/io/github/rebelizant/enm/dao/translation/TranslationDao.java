package io.github.rebelizant.enm.dao.translation;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public interface TranslationDao extends TranslationCrudDao, TranslationQueryDao {
}
