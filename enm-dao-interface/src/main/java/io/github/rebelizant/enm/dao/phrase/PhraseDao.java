package io.github.rebelizant.enm.dao.phrase;

/**
 * CUD stands for create,update,delete
 *
 * @author rebelizant
 *         Created on 20.03.16.
 */
public interface PhraseDao extends PhraseCrudDao, PhraseQueryDao {
}
