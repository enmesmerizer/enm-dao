package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.translation.AbstractTranslation;
import io.github.rebelizant.enm.dao.CrudDao;

/**
 * Interface that gathers operations related for
 * create,update,delete(CUD) operations of the translation entity
 *
 * @author rebelizant
 *         Created on 21.02.16.
 */
public interface TranslationCrudDao extends CrudDao<AbstractTranslation> {
}
