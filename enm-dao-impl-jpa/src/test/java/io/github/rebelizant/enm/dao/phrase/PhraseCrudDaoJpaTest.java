package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;
import io.github.rebelizant.enm.core.phrase.PhraseType;
import io.github.rebelizant.enm.core.phrase.en.EnglishPhrase;
import io.github.rebelizant.enm.dao.AbstractDaoJpaTest;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

/**
 * @author rebelizant
 *         Created on 21.02.16.
 */
public class PhraseCrudDaoJpaTest extends AbstractDaoJpaTest {

    @Test
    public void testFindById() {
        Optional<AbstractPhrase> found = phraseCrudDaoJpa.findById(catPhrase.phrase().getId());
        assertThat(found, notNullValue());
        assertTrue(found.isPresent());
        assertThat(found.get(), equalTo(catPhrase.phrase()));
    }

    @Test
    public void testSavePhrase() {
        AbstractPhrase phrase = new EnglishPhrase();
        phrase.setValue("dog");
        phrase.setPartOfSpeech(PartOfSpeech.NOUN);
        phrase.setTranscription("doeg");
        phrase.setAddedByUser(true);
        phrase.setPhraseType(PhraseType.WORD);

        phraseCrudDaoJpa.save(phrase);

        Optional<AbstractPhrase> saved = phraseQueryDaoJpa.findById(phrase.getId(), Language.ENGLISH);
        assertTrue(saved.isPresent());
        assertThat(saved.get(), equalTo(phrase));
        phraseCrudDaoJpa.delete(phrase);
    }

    @Test
    public void testSavePhrases() throws Exception {
        AbstractPhrase one = new EnglishPhrase();
        one.setValue("one");
        one.setPartOfSpeech(PartOfSpeech.UNKNOWN);
        AbstractPhrase two = new EnglishPhrase();
        two.setValue("two");
        two.setPartOfSpeech(PartOfSpeech.UNKNOWN);

        phraseCrudDaoJpa.save(Arrays.asList(one, two));

        Optional<AbstractPhrase> foundOne = phraseQueryDaoJpa.findById(one.getId(), Language.ENGLISH);
        assertTrue(foundOne.isPresent());
        assertThat(foundOne.get(), equalTo(one));

        Optional<AbstractPhrase> foundTwo = phraseQueryDaoJpa.findById(two.getId(), Language.ENGLISH);
        assertTrue(foundTwo.isPresent());
        assertThat(foundTwo.get(), equalTo(two));
    }

    @Test
    public void testDeletePhrase() throws Exception {
        AbstractPhrase three = new EnglishPhrase();
        three.setValue("three");
        three.setPartOfSpeech(PartOfSpeech.UNKNOWN);

        phraseCrudDaoJpa.save(three);
        Optional<AbstractPhrase> byId = phraseQueryDaoJpa.findById(three.getId(), Language.ENGLISH);
        assertTrue(byId.isPresent());
        assertThat(byId.get(), equalTo(three));

        phraseCrudDaoJpa.delete(three);

        assertThat(phraseQueryDaoJpa.findById(three.getId(), Language.ENGLISH), notNullValue());
        assertFalse(phraseQueryDaoJpa.findById(three.getId(), Language.ENGLISH).isPresent());
    }

    @Test
    public void testDeletePhrases() throws Exception {
        AbstractPhrase one = new EnglishPhrase();
        one.setValue("one1");
        one.setPartOfSpeech(PartOfSpeech.UNKNOWN);
        AbstractPhrase two = new EnglishPhrase();
        two.setValue("two2");
        two.setPartOfSpeech(PartOfSpeech.UNKNOWN);

        phraseCrudDaoJpa.save(Arrays.asList(one, two));

        Optional<AbstractPhrase> foundOne = phraseQueryDaoJpa.findById(one.getId(), Language.ENGLISH);
        assertTrue(foundOne.isPresent());
        assertThat(foundOne.get(), equalTo(one));

        Optional<AbstractPhrase> foundTwo = phraseQueryDaoJpa.findById(two.getId(), Language.ENGLISH);
        assertTrue(foundTwo.isPresent());
        assertThat(foundTwo.get(), equalTo(two));

        phraseCrudDaoJpa.delete(Arrays.asList(one, two));
        assertThat(phraseQueryDaoJpa.findById(one.getId(), Language.ENGLISH), notNullValue());
        assertFalse(phraseQueryDaoJpa.findById(one.getId(), Language.ENGLISH).isPresent());
        assertThat(phraseQueryDaoJpa.findById(two.getId(), Language.ENGLISH), notNullValue());
        assertFalse(phraseQueryDaoJpa.findById(two.getId(), Language.ENGLISH).isPresent());
    }
}