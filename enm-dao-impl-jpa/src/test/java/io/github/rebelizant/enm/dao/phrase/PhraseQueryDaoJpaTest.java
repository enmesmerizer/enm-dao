package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;
import io.github.rebelizant.enm.dao.AbstractDaoJpaTest;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author rebelizant
 *         Created on 19.03.16.
 */
public class PhraseQueryDaoJpaTest extends AbstractDaoJpaTest {

    @Test
    public void testFindById() throws Exception {
        Optional<AbstractPhrase> byId = phraseQueryDaoJpa.findById(catPhrase.phrase().getId(), Language.ENGLISH);
        assertTrue(byId.isPresent());
        assertThat(byId.get(), CoreMatchers.equalTo(catPhrase.phrase()));
    }

    @Test
    public void testFindPhrase() throws Exception {
        Optional<AbstractPhrase> foundCat = phraseQueryDaoJpa.findPhrase(catPhrase.value(), PartOfSpeech.NOUN, Language.ENGLISH);
        assertTrue(foundCat.isPresent());
        assertThat(foundCat.get(), equalTo(catPhrase.phrase()));
    }

    @Test
    public void testFindPhrases() throws Exception {
        Collection<AbstractPhrase> foundPhrases = phraseQueryDaoJpa.findPhrases(catPhrase.value(), Language.ENGLISH);
        assertThat(foundPhrases, notNullValue());
        assertThat(foundPhrases.size(), is(1));
        assertThat(foundPhrases.iterator().next(), equalTo(catPhrase.phrase()));
    }

}