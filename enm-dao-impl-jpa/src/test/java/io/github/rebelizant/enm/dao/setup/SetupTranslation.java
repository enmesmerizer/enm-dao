package io.github.rebelizant.enm.dao.setup;

import io.github.rebelizant.common.testutils.BeforeAfterTestRule;
import io.github.rebelizant.enm.core.factory.translation.TranslationFactory;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.translation.AbstractTranslation;
import io.github.rebelizant.enm.dao.translation.TranslationCrudDao;

/**
 * @author rebelizant
 *         Created on 13.03.16.
 */
public class SetupTranslation extends BeforeAfterTestRule {

    private TranslationCrudDao translationCrudDao;

    private TranslationFactory translationFactory;

    private SetupPhrase sourcePhrase;

    private SetupPhrase destPhrase;
    private AbstractTranslation<AbstractPhrase, AbstractPhrase> translation;

    @Override
    protected void before() throws Exception {
        translation = translationFactory.getTranslation(sourcePhrase.language(), destPhrase.language());
        translation.setSourcePhrase(sourcePhrase.phrase());
        translation.setTranslationPhrase(destPhrase.phrase());
        translationCrudDao.save(translation);
    }

    @Override
    protected void after() throws Exception {
        if (translation == null) {
            translation = translationFactory.getTranslation(sourcePhrase.language(), destPhrase.language());
            translation.setSourcePhrase(sourcePhrase.phrase());
            translation.setTranslationPhrase(destPhrase.phrase());
        }
        translationCrudDao.delete(translation);
    }

    public void init(TranslationFactory translationFactory, TranslationCrudDao translationCrudDao) {
        this.translationCrudDao = translationCrudDao;
        this.translationFactory = translationFactory;
    }

    public SetupTranslation source(SetupPhrase sourcePhrase) {
        this.sourcePhrase = sourcePhrase;
        return this;
    }

    public SetupTranslation dest(SetupPhrase destPhrase) {
        this.destPhrase = destPhrase;
        return this;
    }

    public void save() throws Exception {
        before();
    }

    public void delete() throws Exception {
        after();
    }

    public AbstractTranslation translation() {
        return translation;
    }
}
