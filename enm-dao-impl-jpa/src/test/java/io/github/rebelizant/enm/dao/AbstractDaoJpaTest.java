package io.github.rebelizant.enm.dao;

import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.factory.translation.TranslationFactory;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.translation.AbstractTranslation;
import io.github.rebelizant.enm.dao.phrase.PhraseCrudDao;
import io.github.rebelizant.enm.dao.phrase.PhraseQueryDao;
import io.github.rebelizant.enm.dao.setup.SetupPhrase;
import io.github.rebelizant.enm.dao.setup.SetupTranslation;
import io.github.rebelizant.enm.dao.translation.TranslationCrudDao;
import io.github.rebelizant.enm.dao.translation.TranslationQueryDao;
import io.github.rebelizant.utils.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import java.util.Collection;
import java.util.Optional;

import static io.github.rebelizant.enm.core.phrase.Language.ENGLISH;
import static io.github.rebelizant.enm.core.phrase.Language.UKRAINIAN;
import static io.github.rebelizant.enm.core.phrase.PartOfSpeech.NOUN;

/**
 * @author rebelizant
 *         Created on 13.03.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:enm-dao-impl-jpa-context.xml")
public abstract class AbstractDaoJpaTest {

    @Inject
    protected TranslationCrudDao translationCrudDaoJpa;

    @Inject
    protected TranslationQueryDao translationQueryDaoJpa;

    @Inject
    protected PhraseCrudDao phraseCrudDaoJpa;

    @Inject
    protected PhraseQueryDao phraseQueryDaoJpa;

    @Inject
    protected PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer;

    @Inject
    protected TranslationFactory translationFactory;

    public SetupPhrase catPhrase = new SetupPhrase().language(ENGLISH).value("cat").pos(NOUN);

    public SetupPhrase kitPhrase = new SetupPhrase().language(UKRAINIAN).value("кіт").pos(NOUN);

    public SetupTranslation catKitTran = new SetupTranslation().source(catPhrase).dest(kitPhrase);

    @Before
    public void setupPhrases() throws Exception {
        catPhrase.init(phraseFactoryProducer, phraseCrudDaoJpa, translationCrudDaoJpa);
        kitPhrase.init(phraseFactoryProducer, phraseCrudDaoJpa, translationCrudDaoJpa);
        catKitTran.init(translationFactory, translationCrudDaoJpa);

        catPhrase.value(RandomUtils.randomAlphabet(9));
        kitPhrase.value(RandomUtils.randomAlphabet(9));

        catPhrase.save();
        kitPhrase.save();
    }

    @After
    public void deleteCreatedResources() throws Exception {
        Collection<AbstractTranslation> translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), catPhrase.language(), kitPhrase.language());
        if (translations != null && translations.size() > 0) {
            catKitTran.delete();
        }
        Optional<AbstractPhrase> phrase = phraseQueryDaoJpa.findPhrase(catPhrase.value(), catPhrase.pos(), catPhrase.language());
        if (phrase.isPresent()) {
            catPhrase.delete();
        }
        phrase = phraseQueryDaoJpa.findPhrase(kitPhrase.value(), kitPhrase.pos(), kitPhrase.language());
        if (phrase.isPresent()) {
            kitPhrase.delete();
        }
    }

}
