package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.ukr.UkrainianPhrase;
import io.github.rebelizant.enm.core.translation.AbstractTranslation;
import io.github.rebelizant.enm.dao.AbstractDaoJpaTest;
import io.github.rebelizant.utils.RandomUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static io.github.rebelizant.enm.core.phrase.Language.ENGLISH;
import static io.github.rebelizant.enm.core.phrase.Language.UKRAINIAN;
import static io.github.rebelizant.enm.core.phrase.PartOfSpeech.NOUN;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author rebelizant
 *         Created on 13.03.16.
 */
public class TranslationCrudDaoJpaTest extends AbstractDaoJpaTest {

    @Test
    public void testFindById() throws Exception {
        catKitTran.save();
        Optional<AbstractTranslation> found = translationCrudDaoJpa.findById(catKitTran.translation().getId());
        assertThat(found, notNullValue());
        assertTrue(found.isPresent());
        assertThat(found.get(), equalTo(catKitTran.translation()));
    }

    @Test
    public void testSaveTranslations() throws Exception {
        AbstractTranslation<AbstractPhrase, AbstractPhrase> catToKit = translationFactory.getTranslation(catPhrase.language(), kitPhrase.language());
        catToKit.setSourcePhrase(catPhrase.phrase());
        catToKit.setTranslationPhrase(kitPhrase.phrase());

        AbstractPhrase kit2Phrase = new UkrainianPhrase();
        kit2Phrase.setValue(RandomUtils.randomAlphabet(9));
        kit2Phrase.setPartOfSpeech(NOUN);
        phraseCrudDaoJpa.save(kit2Phrase);

        AbstractTranslation<AbstractPhrase, AbstractPhrase> catToKit2 = translationFactory.getTranslation(catPhrase.language(), UKRAINIAN);
        catToKit2.setSourcePhrase(catPhrase.phrase());
        catToKit2.setTranslationPhrase(kit2Phrase);

        translationCrudDaoJpa.save(Arrays.asList(catToKit, catToKit2));

        Collection<AbstractTranslation> catTrs = translationQueryDaoJpa.findTranslations(catPhrase.value(), ENGLISH, UKRAINIAN);
        assertThat(catTrs, notNullValue());
        assertThat(catTrs.size(), is(2));

        translationCrudDaoJpa.delete(catToKit2);
        translationCrudDaoJpa.delete(catToKit);
        phraseCrudDaoJpa.delete(kit2Phrase);
    }

    @Test
    public void testSaveTranslation() throws Exception {
        AbstractTranslation<AbstractPhrase, AbstractPhrase> translation = translationFactory.getTranslation(catPhrase.language(), kitPhrase.language());
        translation.setSourcePhrase(catPhrase.phrase());
        translation.setTranslationPhrase(kitPhrase.phrase());
        translationCrudDaoJpa.save(translation);

        Collection<AbstractTranslation> translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), catPhrase.language(), kitPhrase.language());
        assertThat(translations, notNullValue());
        assertThat(translations.size(), is(1));
        AbstractTranslation next = translations.iterator().next();
        assertThat(next.getSourcePhrase(), equalTo(catPhrase.phrase()));
        assertThat(next.getTranslationPhrase(), equalTo(kitPhrase.phrase()));

        translationCrudDaoJpa.delete(translation);
    }

    @Test
    public void testDeleteTranslations() throws Exception {
        catKitTran.save();
        Collection<AbstractTranslation> translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), catPhrase.language(), kitPhrase.language());
        assertThat(translations, notNullValue());
        assertThat(translations.size(), is(1));

        translationCrudDaoJpa.delete(Collections.singletonList(catKitTran.translation()));

        translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), catPhrase.language(), kitPhrase.language());
        assertThat(translations, notNullValue());
        assertThat(translations.size(), is(0));
    }

    @Test
    public void testDeleteTranslation() throws Exception {
        catKitTran.save();
        Collection<AbstractTranslation> translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), catPhrase.language(), kitPhrase.language());
        assertThat(translations, notNullValue());
        assertThat(translations.size(), is(1));

        translationCrudDaoJpa.delete(catKitTran.translation());

        translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), catPhrase.language(), kitPhrase.language());
        assertThat(translations, notNullValue());
        assertThat(translations.size(), is(0));
    }
}