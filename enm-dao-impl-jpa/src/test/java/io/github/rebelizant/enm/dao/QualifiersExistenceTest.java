package io.github.rebelizant.enm.dao;

import io.github.rebelizant.enm.dao.qualifier.EnmDaoJPAImpl;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author rebelizant
 *         Created on 19.03.16.
 */
public class QualifiersExistenceTest extends AbstractDaoJpaTest {

    @Test
    public void testQualifiersExistence() {
        verifyEnmDaoJPAImplExist(phraseCrudDaoJpa);
        verifyEnmDaoJPAImplExist(phraseQueryDaoJpa);
        verifyEnmDaoJPAImplExist(translationCrudDaoJpa);
        verifyEnmDaoJPAImplExist(translationQueryDaoJpa);
    }

    public void verifyEnmDaoJPAImplExist(Object obj) {
        String errorMessage = String.format("Class <%s> is not annotated with %s", obj.getClass().getSimpleName(),
                EnmDaoJPAImpl.class.getSimpleName());
        assertThat(errorMessage, obj.getClass().getAnnotation(EnmDaoJPAImpl.class), notNullValue());
    }

}
