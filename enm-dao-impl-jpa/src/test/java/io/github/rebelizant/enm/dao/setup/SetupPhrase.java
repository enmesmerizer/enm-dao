package io.github.rebelizant.enm.dao.setup;

import io.github.rebelizant.common.testutils.BeforeAfterTestRule;
import io.github.rebelizant.enm.core.factory.phrase.PhraseFactory;
import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;
import io.github.rebelizant.enm.dao.phrase.PhraseCrudDao;
import io.github.rebelizant.enm.dao.translation.TranslationCrudDao;
import io.github.rebelizant.utils.RandomUtils;

/**
 * @author rebelizant
 *         Created on 13.03.16.
 */
public class SetupPhrase extends BeforeAfterTestRule {

    private PhraseCrudDao phraseCrudDao;

    private Language language;

    private String value;

    private PartOfSpeech partOfSpeech;

    private PhraseFactoryProducer phraseFactoryProducer;
    private AbstractPhrase phrase;
    private TranslationCrudDao translationCrudDao;

    public SetupPhrase(PhraseFactoryProducer phraseFactoryProducer, PhraseCrudDao phraseCrudDao) {
        this.phraseFactoryProducer = phraseFactoryProducer;
        this.phraseCrudDao = phraseCrudDao;
    }

    public SetupPhrase() {
    }

    @Override
    protected void before() throws Exception {
        phrase = buildPhrase();
        phrase.setValue(phrase.getValue() + RandomUtils.randomAlphabet(6));
        phraseCrudDao.save(phrase);
    }

    @Override
    protected void after() throws Exception {
        if (phrase == null) {
            phrase = buildPhrase();
        }
        phraseCrudDao.delete(phrase);
    }

    private AbstractPhrase buildPhrase() {
        PhraseFactory phraseFactory = phraseFactoryProducer.getPhraseFactory(language);
        phrase = phraseFactory.getPhrase();
        phrase.setValue(value);
        phrase.setPartOfSpeech(partOfSpeech);
        return phrase;
    }

    public SetupPhrase language(Language language) {
        this.language = language;
        return this;
    }

    public SetupPhrase value(String value) {
        this.value = value;
        return this;
    }

    public SetupPhrase pos(PartOfSpeech partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
        return this;
    }

    public Language language() {
        return language;
    }

    public String value() {
        return phrase.getValue();
    }

    public PartOfSpeech pos() {
        return phrase.getPartOfSpeech();
    }

    public AbstractPhrase phrase() {
        return phrase;
    }

    public void init(PhraseFactoryProducer phraseFactoryProducer, PhraseCrudDao phraseCrudDao, TranslationCrudDao translationCrudDao) {
        this.phraseFactoryProducer = phraseFactoryProducer;
        this.phraseCrudDao = phraseCrudDao;
        this.translationCrudDao = translationCrudDao;
    }

    public void save() throws Exception {
        before();
    }

    public void delete() throws Exception {
        after();
    }

}
