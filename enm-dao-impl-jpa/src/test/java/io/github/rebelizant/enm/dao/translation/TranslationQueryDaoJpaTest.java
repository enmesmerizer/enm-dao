package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.translation.AbstractTranslation;
import io.github.rebelizant.enm.dao.AbstractDaoJpaTest;
import org.junit.Test;

import java.util.Collection;
import java.util.Optional;

import static io.github.rebelizant.enm.core.phrase.Language.ENGLISH;
import static io.github.rebelizant.enm.core.phrase.Language.UKRAINIAN;
import static io.github.rebelizant.enm.core.phrase.PartOfSpeech.NOUN;
import static io.github.rebelizant.enm.core.phrase.PartOfSpeech.PARTICIPLE;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author rebelizant
 *         Created on 19.03.16.
 */
public class TranslationQueryDaoJpaTest extends AbstractDaoJpaTest {

    @Test
    public void testFindById() throws Exception {
        catKitTran.save();
        Optional<AbstractTranslation> found = translationQueryDaoJpa.findById(catKitTran.translation().getId(), ENGLISH, UKRAINIAN);
        assertThat(found, notNullValue());
        assertTrue(found.isPresent());
        assertThat(found.get(), equalTo(catKitTran.translation()));
    }

    @Test
    public void testFindTranslations() throws Exception {
        catKitTran.save();

        Collection<AbstractTranslation> translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), catPhrase.language(), kitPhrase.language());
        assertThat(translations, notNullValue());
        assertThat(translations.size(), is(1));
        assertThat(translations.iterator().next(), notNullValue());
        assertThat(translations.iterator().next().getSourcePhrase(), equalTo(catPhrase.phrase()));
        assertThat(translations.iterator().next().getTranslationPhrase(), equalTo(kitPhrase.phrase()));
    }

    @Test
    public void testFindTranslations1() throws Exception {
        catKitTran.save();
        Collection<AbstractTranslation> translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), PARTICIPLE, ENGLISH, UKRAINIAN);
        assertThat(translations, notNullValue());
        assertThat(translations.isEmpty(), is(true));
        translations = translationQueryDaoJpa.findTranslations(catPhrase.value(), NOUN, ENGLISH, UKRAINIAN);
        assertThat(translations, notNullValue());
        assertThat(translations.isEmpty(), is(false));
        assertThat(translations.size(), is(1));
    }

}