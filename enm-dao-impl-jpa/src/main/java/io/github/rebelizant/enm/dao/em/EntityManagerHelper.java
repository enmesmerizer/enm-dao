package io.github.rebelizant.enm.dao.em;

/**
 * @author rebelizant
 *         Created on 13.03.16.
 */
public interface EntityManagerHelper {

    <V> V call(CallableWithEntityManager<V> callable);



    void close();

}
