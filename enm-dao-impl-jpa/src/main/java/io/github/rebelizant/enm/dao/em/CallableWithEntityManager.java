package io.github.rebelizant.enm.dao.em;

import javax.persistence.EntityManager;

/**
 * @author rebelizant
 *         Created on 13.03.16.
 */
public interface CallableWithEntityManager<V> {

    V call(EntityManager entityManager) throws Exception;

}
