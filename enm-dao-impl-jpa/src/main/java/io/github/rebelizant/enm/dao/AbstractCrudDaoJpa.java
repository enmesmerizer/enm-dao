package io.github.rebelizant.enm.dao;

import io.github.rebelizant.enm.core.AbstractEntity;
import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.factory.translation.TranslationFactory;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.dao.em.EntityManagerHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public abstract class AbstractCrudDaoJpa<T extends AbstractEntity> extends AbstractDaoJpa implements CrudDao<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractCrudDaoJpa.class);

    protected AbstractCrudDaoJpa(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, TranslationFactory translationFactory, EntityManagerHelper entityManagerHelper) {
        super(phraseFactoryProducer, translationFactory, entityManagerHelper);
    }

    protected AbstractCrudDaoJpa(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, EntityManagerHelper entityManagerHelper) {
        super(phraseFactoryProducer, entityManagerHelper);
    }

    @Override
    public void save(T entity) {
        entityManagerHelper.call(entityManager -> {
            if (entityManager.find(entity.getClass(), entity.getId()) == null) {
                entityManager.persist(entity);
            } else {
                entityManager.merge(entity);
            }
            return null;
        });
    }

    @Override
    public void save(Collection<T> entities) {
        entityManagerHelper.call((entityManager -> {
            entities.stream().forEach(entity -> {
                if (entityManager.find(entity.getClass(), entity.getId()) == null) {
                    entityManager.persist(entity);
                } else {
                    entityManager.merge(entity);
                }
            });
            return null;
        }));
    }

    @Override
    public void delete(T entity) {
        entityManagerHelper.call((entityManager -> {
            entityManager.remove(entityManager.find(entity.getClass(), entity.getId()));
            return null;
        }));
    }

    @Override
    public void delete(Collection<T> entities) {
        entityManagerHelper.call((entityManager -> {
            entities.stream()
                    .forEach(entity -> entityManager
                            .remove(entityManager.find(entity.getClass(), entity.getId())));
            return null;
        }));
    }
}
