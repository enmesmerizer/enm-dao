package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.dao.AbstractCrudDaoJpa;
import io.github.rebelizant.enm.dao.em.EntityManagerHelper;
import io.github.rebelizant.enm.dao.qualifier.EnmDaoJPAImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * @author rebelizant
 *         Created on 21.02.16.
 */
@EnmDaoJPAImpl
@Named
public class PhraseCrudDaoJpa extends AbstractCrudDaoJpa<AbstractPhrase> implements PhraseCrudDao {

    private static final Logger logger = LoggerFactory.getLogger(PhraseCrudDaoJpa.class);

    public PhraseCrudDaoJpa(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, EntityManagerHelper entityManagerHelper) {
        super(phraseFactoryProducer, entityManagerHelper);
    }

    @Override
    public Optional<AbstractPhrase> findById(Long id) {
        return entityManagerHelper.call(entityManager -> ofNullable(entityManager.find(AbstractPhrase.class, id)));
    }
}
