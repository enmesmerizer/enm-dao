package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;
import io.github.rebelizant.enm.dao.AbstractDaoJpa;
import io.github.rebelizant.enm.dao.em.EntityManagerHelper;
import io.github.rebelizant.enm.dao.qualifier.EnmDaoJPAImpl;

import javax.inject.Named;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 19.03.16.
 */
@EnmDaoJPAImpl
@Named
public class PhraseQueryDaoJpa extends AbstractDaoJpa implements PhraseQueryDao {

    public PhraseQueryDaoJpa(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, EntityManagerHelper entityManagerHelper) {
        super(phraseFactoryProducer, entityManagerHelper);
    }
    @Override
    public Optional<AbstractPhrase> findById(long id, Language language) {
        Class phraseClass = phraseFactoryProducer.getPhraseFactory(language).getPhraseClass();
        return entityManagerHelper.call((entityManager -> Optional.ofNullable((AbstractPhrase) entityManager.find(phraseClass, id))));
    }

    @Override
    public Optional<AbstractPhrase> findPhrase(String phraseValue, PartOfSpeech partOfSpeech, Language language) {
        return entityManagerHelper.call(entityManager -> {
            Class<? extends AbstractPhrase> phraseClass = phraseFactoryProducer.getPhraseFactory(language).getPhraseClass();
            Query query = entityManager.createQuery("select phrase from " + phraseClass.getSimpleName()
                    + " phrase where phrase.value=:value and phrase.partOfSpeech=:partOfSpeech");
            query.setParameter("value", phraseValue);
            query.setParameter("partOfSpeech", partOfSpeech);
            List resultList = query.getResultList();
            return resultList.isEmpty()
                    ? Optional.empty()
                    : Optional.of(phraseClass.cast(resultList.iterator().next()));
        });
    }

    @Override
    public Collection<AbstractPhrase> findPhrases(String phraseValue, Language language) {
        return entityManagerHelper.call(entityManager -> {
            Class<? extends AbstractPhrase> phraseClass = phraseFactoryProducer.getPhraseFactory(language).getPhraseClass();
            Query query = entityManager.createQuery("select phrase from " + phraseClass.getSimpleName()
                    + " phrase where phrase.value=:value");
            query.setParameter("value", phraseValue);
            @SuppressWarnings("unchecked")
            Collection<AbstractPhrase> result = query.getResultList();
            return result;
        });
    }

}
