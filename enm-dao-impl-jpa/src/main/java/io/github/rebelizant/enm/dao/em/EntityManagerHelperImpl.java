package io.github.rebelizant.enm.dao.em;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 13.03.16.
 */
public class EntityManagerHelperImpl implements EntityManagerHelper {

    private static final Logger logger = LoggerFactory.getLogger(EntityManagerHelperImpl.class);

    private EntityManagerFactory entityManagerFactory;

    public EntityManagerHelperImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public <V> V call(CallableWithEntityManager<V> callable) {
        EntityManager entityManager = null;
        EntityTransaction transaction = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();
            V result = callable.call(entityManager);
            transaction.commit();
            logger.debug("Transaction is successful.");
            return result;
        } catch (Exception e) {
            logger.debug("Transaction has failed.", e);
            Optional.ofNullable(transaction).ifPresent(EntityTransaction::rollback);
            throw new RuntimeException(e);
        } finally {
            logger.debug("Closing entity manager.");
            Optional.ofNullable(entityManager).ifPresent(EntityManager::close);
        }
    }

    @Override
    public void close() {
        logger.debug("Closing EntityManagerFactory");
        Optional.ofNullable(entityManagerFactory).ifPresent(EntityManagerFactory::close);
    }
}
