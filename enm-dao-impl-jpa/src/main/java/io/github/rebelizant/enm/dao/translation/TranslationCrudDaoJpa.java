package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.factory.translation.TranslationFactory;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.translation.AbstractTranslation;
import io.github.rebelizant.enm.dao.AbstractCrudDaoJpa;
import io.github.rebelizant.enm.dao.em.EntityManagerHelper;
import io.github.rebelizant.enm.dao.qualifier.EnmDaoJPAImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * @author rebelizant
 *         Created on 21.02.16.
 */
@EnmDaoJPAImpl
@Named
public class TranslationCrudDaoJpa extends AbstractCrudDaoJpa<AbstractTranslation> implements TranslationCrudDao {

    private static final Logger logger = LoggerFactory.getLogger(TranslationCrudDaoJpa.class);

    public TranslationCrudDaoJpa(EntityManagerHelper entityManagerHelper, PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, TranslationFactory translationFactory) {
        super(phraseFactoryProducer, translationFactory, entityManagerHelper);
    }

    @Override
    public Optional<AbstractTranslation> findById(Long id) {
        return entityManagerHelper.call(entityManager -> ofNullable(entityManager.find(AbstractTranslation.class, id)));
    }
}
