package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.factory.translation.TranslationFactory;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;
import io.github.rebelizant.enm.core.translation.AbstractTranslation;
import io.github.rebelizant.enm.dao.AbstractDaoJpa;
import io.github.rebelizant.enm.dao.em.EntityManagerHelper;
import io.github.rebelizant.enm.dao.phrase.PhraseQueryDao;
import io.github.rebelizant.enm.dao.qualifier.EnmDaoJPAImpl;

import javax.inject.Named;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author rebelizant
 *         Created on 19.03.16.
 */
@EnmDaoJPAImpl
@Named
public class TranslationQueryDaoJpa extends AbstractDaoJpa implements TranslationQueryDao {

    protected PhraseQueryDao phraseQueryDao;

    public TranslationQueryDaoJpa(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, TranslationFactory translationFactory, EntityManagerHelper entityManagerHelper, PhraseQueryDao phraseQueryDao) {
        super(phraseFactoryProducer, translationFactory, entityManagerHelper);
        this.phraseQueryDao = phraseQueryDao;
    }

    @Override
    public Optional<AbstractTranslation> findById(Long id, Language srcLang, Language destLang) {
        return entityManagerHelper.call(entityManager -> {
            Class<AbstractTranslation> translationClass = translationFactory.getTranslationClass(srcLang, destLang);
            return Optional.ofNullable(entityManager.find(translationClass, id));
        });
    }

    @Override
    public Collection<AbstractTranslation> findTranslations(String phraseValue, Language srcLang, Language destLang) {
        return entityManagerHelper.call(entityManager -> phraseQueryDao.findPhrases(phraseValue, srcLang)
                .stream().flatMap(phrase -> {
                    Class<? extends AbstractTranslation> aClass = translationFactory.getTranslationClass(srcLang, destLang);
                    Query query = entityManager.createQuery("select translation from "
                            + aClass.getSimpleName() + " translation where translation.sourcePhrase.id=:id");
                    query.setParameter("id", phrase.getId());
                    @SuppressWarnings("unchecked")
                    Collection<AbstractTranslation> resultList = query.getResultList();
                    return resultList.stream();
        }).collect(Collectors.toList()));
    }

    @Override
    public Collection<AbstractTranslation> findTranslations(String phraseValue, PartOfSpeech partOfSpeech, Language srcLang, Language destLang) {
        return entityManagerHelper.call(entityManager -> {
            Optional<AbstractPhrase> optionalPhrase = phraseQueryDao.findPhrase(phraseValue, partOfSpeech, srcLang);
            if (optionalPhrase.isPresent()) {
                Class<? extends AbstractTranslation> translationClass = translationFactory.getTranslationClass(srcLang, destLang);
                Query query = entityManager.createQuery("select translation from "
                        + translationClass.getSimpleName() + " translation where translation.sourcePhrase.id=:id and "
                        + " translation.translationPhrase.partOfSpeech=:partOfSpeech");
                query.setParameter("id", optionalPhrase.get().getId());
                query.setParameter("partOfSpeech", partOfSpeech);
                @SuppressWarnings("unchecked")
                Collection<AbstractTranslation> resultList = query.getResultList();
                return resultList;
            } else {
                return Collections.emptyList();
            }
        });
    }
}
