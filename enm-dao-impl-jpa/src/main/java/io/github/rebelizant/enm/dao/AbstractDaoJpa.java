package io.github.rebelizant.enm.dao;

import io.github.rebelizant.enm.core.factory.phrase.PhraseFactoryProducer;
import io.github.rebelizant.enm.core.factory.translation.TranslationFactory;
import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.dao.em.EntityManagerHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rebelizant
 *         Created on 19.03.16.
 */
public abstract class AbstractDaoJpa {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDaoJpa.class);

    protected PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer;

    protected TranslationFactory translationFactory;

    protected EntityManagerHelper entityManagerHelper;

    protected AbstractDaoJpa(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, TranslationFactory translationFactory, EntityManagerHelper entityManagerHelper) {
        this.phraseFactoryProducer = phraseFactoryProducer;
        this.translationFactory = translationFactory;
        this.entityManagerHelper = entityManagerHelper;
    }

    protected AbstractDaoJpa(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer, EntityManagerHelper entityManagerHelper) {
        this.phraseFactoryProducer = phraseFactoryProducer;
        this.entityManagerHelper = entityManagerHelper;
    }

    protected void setPhraseFactoryProducer(PhraseFactoryProducer<AbstractPhrase> phraseFactoryProducer) {
        this.phraseFactoryProducer = phraseFactoryProducer;
    }

    protected void setTranslationFactory(TranslationFactory translationFactory) {
        this.translationFactory = translationFactory;
    }

    protected void setEntityManagerHelper(EntityManagerHelper entityManagerHelper) {
        this.entityManagerHelper = entityManagerHelper;
    }
}
