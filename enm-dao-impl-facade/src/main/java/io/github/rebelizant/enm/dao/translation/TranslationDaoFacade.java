package io.github.rebelizant.enm.dao.translation;

import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;
import io.github.rebelizant.enm.core.translation.AbstractTranslation;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public class TranslationDaoFacade implements TranslationDao {

    private TranslationCrudDao translationCrudDao;

    private TranslationQueryDao translationQueryDao;

    public TranslationDaoFacade(TranslationCrudDao translationCrudDao, TranslationQueryDao translationQueryDao) {
        this.translationCrudDao = translationCrudDao;
        this.translationQueryDao = translationQueryDao;
    }

    @Override
    public void save(Collection<AbstractTranslation> translations) {
        translationCrudDao.save(translations);
    }

    @Override
    public Optional<AbstractTranslation> findById(Long id) {
        return translationCrudDao.findById(id);
    }

    @Override
    public void save(AbstractTranslation translation) {
        translationCrudDao.save(translation);
    }

    @Override
    public void delete(Collection<AbstractTranslation> translations) {
        translationCrudDao.delete(translations);
    }

    @Override
    public void delete(AbstractTranslation translation) {
        translationCrudDao.delete(translation);
    }

    @Override
    public Optional<AbstractTranslation> findById(Long id, Language srcLang, Language destLang) {
        return translationQueryDao.findById(id, srcLang, destLang);
    }

    @Override
    public Collection<AbstractTranslation> findTranslations(String phraseValue, Language srcLang, Language destLang) {
        return translationQueryDao.findTranslations(phraseValue, srcLang, destLang);
    }

    @Override
    public Collection<AbstractTranslation> findTranslations(String phraseValue, PartOfSpeech partOfSpeech, Language srcLang, Language destLang) {
        return translationQueryDao.findTranslations(phraseValue, partOfSpeech, srcLang, destLang);
    }
}
