package io.github.rebelizant.enm.dao.phrase;

import io.github.rebelizant.enm.core.phrase.AbstractPhrase;
import io.github.rebelizant.enm.core.phrase.Language;
import io.github.rebelizant.enm.core.phrase.PartOfSpeech;

import java.util.Collection;
import java.util.Optional;

/**
 * @author rebelizant
 *         Created on 20.03.16.
 */
public class PhraseDaoFacade implements PhraseDao {

    private PhraseCrudDao phraseCrudDao;

    private PhraseQueryDao phraseQueryDao;

    public PhraseDaoFacade(PhraseCrudDao phraseCrudDao, PhraseQueryDao phraseQueryDao) {
        this.phraseCrudDao = phraseCrudDao;
        this.phraseQueryDao = phraseQueryDao;
    }

    @Override
    public Optional<AbstractPhrase> findById(Long id) {
        return phraseCrudDao.findById(id);
    }

    @Override
    public void save(AbstractPhrase phrase) {
        phraseCrudDao.save(phrase);
    }

    @Override
    public void save(Collection<AbstractPhrase> phrases) {
        phraseCrudDao.save(phrases);
    }

    @Override
    public void delete(AbstractPhrase phrase) {
        phraseCrudDao.delete(phrase);
    }

    @Override
    public void delete(Collection<AbstractPhrase> phrases) {
        phraseCrudDao.delete(phrases);
    }

    @Override
    public Optional<AbstractPhrase> findById(long id, Language language) {
        return phraseQueryDao.findById(id, language);
    }

    @Override
    public Optional<AbstractPhrase> findPhrase(String phraseValue, PartOfSpeech partOfSpeech, Language language) {
        return phraseQueryDao.findPhrase(phraseValue, partOfSpeech, language);
    }

    @Override
    public Collection<AbstractPhrase> findPhrases(String phraseValue, Language language) {
        return phraseQueryDao.findPhrases(phraseValue, language);
    }
}
